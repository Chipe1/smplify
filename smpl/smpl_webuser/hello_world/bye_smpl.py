from smpl_webuser.serialization import load_model
import numpy as np
import chumpy as ch
from smpl_webuser.lbs import global_rigid_transformation

## Load SMPL model (here we load the female model)
## Make sure path is correct
m = load_model( '../../models/basicModel_f_lbs_10_207_0_v1.0.0.pkl' )

## Assign random pose and shape parameters
m.pose[:] = np.random.rand(m.pose.size) * .0
m.betas[:] = np.random.rand(m.betas.size) * .0

opt_pose=ch.array(m.pose.r)
(_, A_global) = global_rigid_transformation(
    opt_pose, m.J, m.kintree_table, xp=ch)
Jtr = ch.vstack([g[:3, 3] for g in A_global])
smpl_ids = [8, 5, 2, 1, 4, 7, 21, 19, 17, 16, 18, 20]
Jtr = Jtr[smpl_ids].r


## Write to an .obj file
outmesh_path = './bye_smpl.obj'
with open( outmesh_path, 'w') as fp:
    for v in Jtr:
        fp.write( 'v %f %f %f\n' % ( v[0], v[1], v[2]) )

## Print message
print '..Output mesh saved to: ', outmesh_path 
